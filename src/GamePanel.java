import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;
/**
 * le plateau de jeu
 * @author imad
 *
 */
public class GamePanel extends JPanel implements MouseListener, MouseMotionListener {
	/**
	 * commence un nouveau jeu
	 */
	
	Game game = new Game();
	//background forest
	private ImageIcon forestBackGround = new ImageIcon("images//BG.PNG");
	private ImageIcon startScreen = new ImageIcon("images//StartScreen.gif");
	private ImageIcon teamSelectScreen = new ImageIcon("images//TeamSelectScreen.gif");
	private ImageIcon aboutusScreen = new ImageIcon("images//TeamAboutUs.gif");
	
	private ImageIcon whiteHighlight = new ImageIcon("images//whiteHighlight.PNG");
	private ImageIcon victoryBorder = new ImageIcon("images//VictoryBorder.PNG");
	
	//board info
	private int tour = 2;
	private int numCols = 16;
	private int numRows = 11;
	private int boardWidth = 1200;
	private int boardHeight = 949;
	private int colWidth = 75;
	private int rowHeight = 79;
	public int northWestCornerX = 25;
	public int northWestCornerY = 25;
	public int northEastCornerX = 1225;
	public int northEastCornerY = 25;
	public int southWestCornerX = 25;
	public int southWestCornerY = 974;
	public int southEastCornerX = 1225;
	public int southEastCornerY = 974;
	
	private int attackerX =0;
	private int attackerY =0;
	private int highlightX = 0;
	private int highlightY = 0;
	private boolean highlight = false;
	private int abilityX = 0;
	private int abilityY = 0;
	private boolean highlightStartGame = false;
	private boolean highlightaboutus = false;
	private boolean highlightload = false;
	private boolean highlightTeamSelect = false;
	private boolean highlightBack = false;
	private boolean highlightBackGame = false;
	private boolean highlightSave = false;
	private boolean ability = false;
	private boolean moveStarted = false;
	private boolean attackStarted = false;
	private boolean teamSelect = false;
	private boolean aboutus = false;
	private boolean loadGame = false;
	private boolean win = false;
	private boolean loose = false;
	private int spriteAnimation = 0;
	private int velocity = 1;
	Color backGround = new Color(79, 79, 79);
	Color outline = new Color(50, 50, 50);
	private GamePiece markedPiece;
	private boolean markPiece = false;
	private boolean moveOrAttack = false;
	private boolean endgame = false;
	private boolean gamesave = false;
	
	//timer actionlistener
	ActionListener timeTicked = new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent ae) {
	        if(spriteAnimation == -3) {
	        	velocity = 1;
	        }
	        else if(spriteAnimation == 3){
	        	velocity = -1;
	        }
	        spriteAnimation += velocity;
	        repaint();
	    }
	};
	//timer with tickrate of 100ms
	private Timer timer= new Timer(100, timeTicked);


	/**
	 * gamePanel constructor
	 */
	public GamePanel()  {
		setPreferredSize(new Dimension(1900, 1000));
		setBackground(backGround);
		this.addMouseListener(this);
		addMouseMotionListener(this);
		timer.start();
		Game.playSound("images//The Horn of Erebor.wav");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Game.playSound("images//javagameost.wav");
	}
	
	/**
	 * l'essentiel du travail graphique - prend l'objet graphique et peint tout sur le canevas
	 */
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		if(!game.isBegun() && teamSelect == false && aboutus == false) {
			
			g.drawImage(startScreen.getImage(), 0, 0, 1900, 1000, this);
			if(highlightStartGame) {
				g.drawImage(whiteHighlight.getImage(), 250, 360, 350, 80, this);
			}
			else if(highlightTeamSelect) {
				g.drawImage(whiteHighlight.getImage(), 250, 500, 395, 80, this);
			}
			
			else if(highlightaboutus) {
				g.drawImage(whiteHighlight.getImage(), 270, 670, 290, 80, this);
			}
			
			else if(highlightload) {
				g.drawImage(whiteHighlight.getImage(), 280, 830, 280, 80, this);
			}
			
			
		}
		else if(!game.isBegun() && teamSelect) {
			g.drawImage(teamSelectScreen.getImage(), 0, 0, 1900, 1000, this);
			if(highlightBack) {
				g.drawImage(whiteHighlight.getImage(), 50, 30, 245, 70, this);
				game.restart();
				
			}
			if(highlight) {
				int x = (((highlightX-85)/92)*92) + 75;
				int y = ((highlightY-220)/140)*140 + 210;
				g.drawImage(whiteHighlight.getImage(), x, y, 92, 140, this);
				
			}
			/**
			 * Coordonn�es de d�part de la premi�re option
			 */
			int x = 85;
			int y = 220;
			/**
			 * dessine toutes les options
			 */
			for(int i = 0; i<game.characters.length; ++i) {
				GamePiece p =  game.characters[i];
				g.setColor(Color.green);
				g.fillRect(x, y, (p.getHealth()*70)/p.getMaxHealth(), 11);
				g.setColor(Color.black);
				g.setFont(new Font("Arial", 10, 9));
				g.drawString(Integer.toString(p.getHealth()) + " / " + Integer.toString(p.getMaxHealth()), x+2, y+9);
				g.setFont(new Font("Arial", 10, 10));
				g.setColor(Color.white);
				g.drawString(p.getName(), x, 95 + y);
				g.drawString("Lvl: " + Integer.toString(p.getLevel()), x, 106 + y);
				g.drawString("Dmg: " + Integer.toString(p.getDamage()), 30 + x, 106 + y);
				if(p.isSelected()) {
					g.drawImage(whiteHighlight.getImage(), x-10, y-10, 92, 140, this);
				}
				g.drawImage(p.getImage().getImage(),3 + x, 25 + y - spriteAnimation, 60, 60+spriteAnimation, this);
				g.drawString("XP: " + Integer.toString(p.getXP()), x, 117 + y);
				/**
				 * modifier cette information lors de l'ajout de nouvelles unit�s
				 */
				if((i+1)%3==0) {
					x = x + 184;
				}
				else {
					x = x + 92;
				}
				if((i+1)%15 == 0) {
					x = 85;
					y = y + 140;
				}
			}
		}
		
		
		else if(!game.isBegun() && aboutus) {
			g.drawImage(aboutusScreen.getImage(), 0, 0, 1900, 1000, this);
			if(highlightBack) {
				g.drawImage(whiteHighlight.getImage(), 50, 30, 245, 70, this);
				
			}
			if(highlight) {
				int x = (((highlightX-85)/92)*92) + 75;
				int y = ((highlightY-220)/140)*140 + 210;
				g.drawImage(whiteHighlight.getImage(), x, y, 92, 140, this);
				
			}
  			/**
			 * dessine toutes les options
			 */
			}
		
		
		
		
		
		
		
		
		else if(game.isBegun()) {


			// background forest
			g.drawImage(forestBackGround.getImage(), northWestCornerX, northWestCornerY, boardWidth, boardHeight, this);
			
			//thicker outlines
			g.setColor(outline);
			g.fillRect(northWestCornerX-1, northWestCornerY-1, boardWidth+2, 2); //top side
			g.fillRect(southWestCornerX-1, southWestCornerY-1, boardWidth+2, 2); //bottom side
			g.fillRect(northWestCornerX-1, northWestCornerY-1, 2, boardHeight+2); //left side
			g.fillRect(northEastCornerX, northEastCornerY-1, 2, boardHeight+2); //right side
		
			//character info slot
			g.fillRect(1700, 25, 175, 320);
			g.fillRect(1700, 354, 175, 70);
			
			//move history background
			g.fillRect(1255, 25, 420, 400);
			g.setColor(Color.white);
			g.setFont(new Font("Arial", 12, 12));
			g.drawString("Move History", 1270, 47);
			for(int i = 0; i<game.getMoves().size(); ++i) {
				g.drawString(game.getMoves().get(i), 1270, 63+14*i);
			}
			
			g.setColor(outline);
			
			//scorechart
			g.fillRect(1700, 450, 175, 190);
			g.fillRect(1700, 650, 175, 190);
			g.fillRect(1700, 850, 175, 125);
			g.setColor(Color.white);
			g.setFont(new Font("Arial", 12, 12));
			g.drawString("Blue", 1710, 470);
			g.drawString("Total Damage Dealt: " + game.blueDamageDealt(), 1710, 486);
			g.setColor(Color.red);
			g.fillRect(1710, 492, 152, 15);
			g.setColor(Color.green);
			g.fillRect(1710, 492, (game.blueCurTotalHealth()*152)/game.blueMaxTotalHealth(), 15);
			g.setColor(Color.white);
			g.drawString("Red", 1710, 670);
			g.drawString("Total Damage Dealt: " + game.redDamageDealt(), 1710, 686);
			g.setColor(Color.red);
			g.fillRect(1710, 692, 152, 15);
			g.setColor(Color.green);
			try {
				g.fillRect(1710, 692, (game.redCurTotalHealth()*152)/game.redMaxTotalHealth(), 15);
			}catch(Exception e) {
				
			}
			
			g.setColor(Color.white);
			g.drawString("Sauvegarder", 1752, 885);
			g.drawString("Retourner", 1760, 945);
			
			
			// grid
			g.setColor(outline); 
			//colonnes o� 100 est le d�calage de la premi�re case (25+75) --> (NWCorner + colWidth)
			for (int i = 0; i < numCols; i++) {
				g.drawLine(100 + (colWidth * i), northWestCornerY, 100 + (colWidth * i), southWestCornerY);
			}
			/**
			 * rangs o� 104 est le d�calage de la premi�re case (25+79) --> (NWCorner + rowHeight)
			 */
			for (int j = 0; j < numRows; j++) {
				g.drawLine(northWestCornerX, 104 + (rowHeight * j), northEastCornerX, 104 + (rowHeight * j));
			}
			
			/**
			 * draw numbers for reference
			 */
			g.setColor(Color.white);
			g.setFont(new Font("Arial", 10, 10));
			for (int i = 0; i < 16; i++) {
				for (int j = 0; j < 12; j++) {
					g.drawString(i + "," + j, 55 + (75 * i), 70 + (79 * j));
				}
			}
			
			if(highlightSave) {
				g.drawImage(whiteHighlight.getImage(), 1700, 850, 175, 62, this);
			}
			if(highlightBackGame) {
				g.drawImage(whiteHighlight.getImage(), 1700, 912, 175, 63, this);
			}
			//highlightPlace
			if(highlight) {
				g.drawImage(whiteHighlight.getImage(), northWestCornerX+1 + ((highlightX-northWestCornerX)/colWidth)*colWidth, northWestCornerY+1 + ((highlightY-northWestCornerY)/rowHeight)*rowHeight, colWidth-1, rowHeight-1, this);
				
			}
			
			//highlightPlace
			if(markPiece) {
				g.drawImage(whiteHighlight.getImage(), 26 + markedPiece.getX()*75, 26 + markedPiece.getY()*79, 74, 78, this);
				for(int i=northWestCornerX+1;i<northEastCornerX;i=i+colWidth) {
					for(int j=northWestCornerY-rowHeight+1;j<=southEastCornerY-rowHeight;j=j+rowHeight) {
						if((game.isValidAttack(markedPiece,game.getPiece(i,j)) || game.getPiece(i,j) == null) && game.isValidMove(i, j, markedPiece) && onBoard(i, j)) {
							g.drawImage(whiteHighlight.getImage(), i, j, 74, 78, this);
						}
					}
				}
				
			}	
			
			//moveOrAttackButtons
//			if(moveOrAttack) {
				if(game.getTurn()==0) {
					g.setColor(Color.blue);
					g.fillRect(1706, 372, 162, 45);
					g.setColor(Color.white);
					g.drawString("TOUR DE :", 1706, 367);
				}else {
					g.setColor(Color.red);
					g.fillRect(1706, 372, 162, 45);
					g.setColor(Color.white);
					g.drawString("TOUR DE :", 1706, 367);
				}
				
//				g.drawString("Move", 1706, 367);
//				
//				g.drawString("Attack", 1791, 367);
//				g.fillRect(1791, 372, 77, 45);
//			}
			
			
			//ability
			if(ability && game.getPiece(highlightX, highlightY) != null) {
				g.drawImage(whiteHighlight.getImage(), 26 + ((highlightX-25)/75)*75, 26 + ((highlightY-25)/79)*79, 74, 78, this);
				GamePiece temp = game.getPiece(abilityX, abilityY);
				g.drawImage(temp.getImage().getImage(), 1755, 32, 50, 50, this);
				g.setColor(Color.white);
				g.setFont(new Font("Arial", 12, 12));
				g.drawString(temp.getName(), 1708, 96);
				g.drawString("Ability: " + temp.getAbilityName(), 1708, 116);
				for(int i = 0; i < temp.getAbilityDescription().length; i++)
				g.drawString(temp.getAbilityDescription()[i], 1708, 129+(12*i));
			}
			

			
			//healthbars, levels, attack, and characters
			for(GamePiece p: game.board) {
				if(p != null && p.isAlive()) {
					g.setColor(Color.red);
					g.fillRect(32 + p.getX()*75, 29 + p.getY()*79, 60, 11);
					g.setColor(Color.green);
					g.fillRect(32 + p.getX()*75, 29 + p.getY()*79, (p.getHealth()*60)/p.getMaxHealth(), 11);
					g.setColor(Color.black);
					g.setFont(new Font("Arial", 10, 9));
					g.drawString(Integer.toString(p.getHealth()) + " / " + Integer.toString(p.getMaxHealth()), 34 + p.getX()*75, 38 + p.getY()*79);
					g.setFont(new Font("Arial", 9, 9));
					g.setColor(Color.white);
					g.drawString("Lvl: " + Integer.toString(p.getLevel()), 29 + p.getX()*75, 100 + p.getY()*79);
					g.drawString("Dmg: " + Integer.toString(p.getDamage()), 58 + p.getX()*75, 100 + p.getY()*79);
					g.drawImage(p.getImage().getImage(),36 + p.getX()*75, 43 + p.getY()*79 - spriteAnimation, 50, 50+spriteAnimation, this);
				}
			}
		}
		//draw victory border and paint winner
		if(game.isOver()) {
			g.setColor(Color.white);
			g.setFont(new Font("Arial", 100, 100));
			g.drawImage(victoryBorder.getImage(),412, 212, 875, 575, this);
			g.drawString(game.getWinner() + " Wins!", 600, 400);
			if(!endgame) {
				for(GamePiece p: game.board) {
					if(p!=null)
					p.setXP(p.getXP() + 1);
				}
				try {
					game.writeGame();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
			endgame = true;
		}
	}
	
	/**
	 * renvoie vrai si les coordonn�es pass�es sont dans les limites de la carte 12x16.
	 * @param x coordonn�e x
	 * @param y coordonn�e y
	 * @return boolean
	 */
	private boolean onBoard(int x, int y) {
		return(x > northWestCornerX && y > northWestCornerY && x <southEastCornerX && y < southEastCornerY);
	}

	//what to do if mouse is clicked
	public void mouseClicked(MouseEvent e) {
		//si le jeu n'est pas encore commenc�
		if(!game.isBegun()) {
			//si le joueur clique sur d�marrer le jeu
			if(!loadGame && !aboutus && !teamSelect && e.getX() > 255 && e.getX() < 600 && e.getY() > 360 && e.getY() < 435) {
				game.beginGame();
			}
			//si le joueur clique sur choisir l'�quipe
			else if(!loadGame && !aboutus && !teamSelect && e.getX() > 260 && e.getX() < 625 && e.getY() > 520 && e.getY() < 570) {
				//go to choose team page
				teamSelect = true;
			}
			
			//si le joueur clique sur ABOUT US
			else if(!loadGame && !aboutus && !teamSelect && e.getX() > 278 && e.getX() < 567 && e.getY() > 674 && e.getY() < 732) {
				//go to ABOUT US page
				aboutus = true;
				
			}
			else if(!loadGame &&!aboutus && !teamSelect && e.getX() > 280 && e.getX() < 560 && e.getY() > 830 && e.getY() < 900) {
				//go to ABOUT US page
				game.beginGame();
				loadGame = true;
				try {
					game.loadGame(game.board);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			
			
			//si sort de aboutus page
			else if(aboutus && e.getX() > 50 && e.getX() < 295 && e.getY() > 30 && e.getY() < 100) {
				//sets aboutus to be false
				highlightBack = false;
				aboutus = false;
			}
			
			//si sort de teamselect
			else if(teamSelect && e.getX() > 50 && e.getX() < 295 && e.getY() > 30 && e.getY() < 100) {
				//sets teamselect to be false
				highlightBack = false;
				teamSelect = false;
				
			}
			else if(teamSelect && e.getY() < 920 && e.getY() > 220 && e.getX() > 50 && e.getX() < 1850 && (((e.getX()-85)/92)+1)%4 != 0) {
				game.select((((e.getX()-85)/92)), ((e.getY()-220)/140), game.characters);
			}
		}
		//si le jeu est d�j� commenc�
		else if(game.isBegun()){
			
			//Sauvegarde de la partie
			if(e.getX() > 1700 && e.getX() < 1875 && e.getY() > 850 && e.getY() < 912) {
				try {
					game.saveGame();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			if(e.getX() > 1700 && e.getX() < 1875 && e.getY() > 913 && e.getY() < 975) {
				game.isBack();
				loadGame = false;
				
				
			}
			
			/**
			 * si le joueur clique sur un emplacement sur le plateau avec une pi�ce et n'a pas 
			 * encore marqu� une pi�ce ou commenc� un d�placement.
			 */
			if(game.getPiece(e.getX(),e.getY()) != null && moveStarted == false && attackStarted == false && game.getPiece(e.getX(),e.getY()).getTeamAsInt() == game.getTurn()) {
				//player will start the move
				moveOrAttack = true;
				markedPiece = game.markPiece(e.getX(), e.getY());
				markPiece = true;
				attackerX =e.getX();
				attackerY =e.getY();
				
			}

			/**
			 * si le joueur a choisi d'attaquer et clique sur un point valide sur lequel se trouve un joueur oppos�.
			 */
			else if(moveOrAttack == false && attackStarted && game.getPiece(e.getX(),e.getY()) != null && game.isValidAttack(markedPiece,game.getPiece(e.getX(),e.getY()))) {
				game.attackPiece(e.getX(), e.getY(),markedPiece.getX(),markedPiece.getY());
				resetMarkers();
			}
			/**
			 * moveOrAttack == false && moveStarted && mettez ceci si vous voulez que le bouton de d�placement fonctionne vers le bas.
			 * si le joueur a choisi de se d�placer et clique sur un emplacement valide qui n'a pas d�j� une pi�ce dessus.
			 */
			
			else if( markedPiece !=null && game.getPiece(e.getX(),e.getY()) == null && game.isValidMove(e.getX(), e.getY(), markedPiece) && onBoard(e.getX(), e.getY())) {
				Game.playSound("images//move.wav");
				moveStarted = true;
				game.movePiece(e.getX(), e.getY());
				tour--;
				if (tour==0) {
					game.update();
					tour=2;
				}
				resetMarkers();
			}
			
			/**
			 * moveOrAttack == false && moveStarted && m�me chose ici 
			 * si le joueur a choisi de se d�placer et clique sur un endroit non valide
			 */
			else if((game.getPiece(e.getX(),e.getY()) != null && game.isValidMove(e.getX(), e.getY(), markedPiece)) && onBoard(e.getX(), e.getY())&& markedPiece !=null)  {
				//game.updateMoves(e.getX(), e.getY(), markedPiece, 1);
				
				Game.playSound("images//kill.wav");
				attackStarted = true;
				
				game.attackPiece(e.getX(), e.getY(), attackerX,attackerY);
				
				
				resetMarkers();
			}
			//si le joueur a choisi d'attaquer et clique sur un point non valide
			else if(moveOrAttack == false && attackStarted && (game.getPiece(e.getX(),e.getY()) != null || !game.isValidAttack(markedPiece,game.getPiece(e.getX(),e.getY()))) && onBoard(e.getX(), e.getY())) {
				game.updateMoves(e.getX(), e.getY(), markedPiece, 1);
				resetMarkers();
			}
			else {
				resetMarkers();
			}
		}
		//si le jeu est termin�
		if(game.isOver()) {
			if(e.getX() > 560 && e.getX() < 1360 && e.getY() > 560 && e.getY() < 670) {
				game.restart();
				endgame = false;
			}
		}
		repaint();
	}
	
	/**
	 * r�initialise tous les marqueurs de l'�tat de la carte.
	 */
	public void resetMarkers() {
		moveStarted = false;
		markPiece = false;
		attackStarted = false;
		moveOrAttack = false;
		attackerX=0;
		attackerY=0;
	}
	
	/**
	 * mouse movement listener
	 */
	public void mouseMoved(MouseEvent e) {

		//si elle est d�plac�e � bord
		if(!game.isBegun()) {
			if(!aboutus && !teamSelect) {
				if(e.getX() > 255 && e.getX() < 600 && e.getY() > 360 && e.getY() < 435) {
					highlightStartGame = true;
					highlightTeamSelect = false;
					highlightaboutus = false;
					highlightload = false;
				}
				else if(e.getX() > 260 && e.getX() < 625 && e.getY() > 520 && e.getY() < 570) {
					highlightTeamSelect = true;
					highlightStartGame = false;
					highlightaboutus = false;
					highlightload = false;
				}
				
				
				else if(e.getX() > 278 && e.getX() < 567 && e.getY() > 674 && e.getY() < 732) {
					highlightaboutus = true;
					highlightTeamSelect = false;
					highlightStartGame = false;
					highlightload = false;
				}
				
				else if(e.getX() > 280 && e.getX() < 560 && e.getY() > 830 && e.getY() < 900) {
					highlightload = true;
					highlightTeamSelect = false;
					highlightStartGame = false;
					highlightaboutus = false;
				}
				
				else {
					highlightaboutus = false;
					highlightStartGame = false;
					highlightTeamSelect = false;
					highlightload = false;
				}
			}
			//si sur l'�cran de s�lection de l'�quipe
			else if(teamSelect) {
				//si dans la zone de s�lection des caract�res
				if(e.getY() < 500 && e.getY() > 220 && e.getX() > 50 && e.getX() < 1850 && (((e.getX()-85)/92)+1)%4 != 0) {
					highlightX = e.getX();
					highlightY = e.getY();
					highlight = true;
					highlightBack = false;
				}
				//si vous cliquez sur le bouton retour
				else if(e.getX() > 50 && e.getX() < 295 && e.getY() > 30 && e.getY() < 100) {
					highlightBack = true;
					highlight = false;
					highlightStartGame = false;
					highlightTeamSelect = false;
				}
				else {
					highlight = false;
					highlightBack = false;
				}
			}
			
			else if(aboutus) {

				//si vous cliquez sur le bouton retour
				if(e.getX() > 50 && e.getX() < 295 && e.getY() > 30 && e.getY() < 100) {
					highlightBack = true;
					highlight = false;
					highlightStartGame = false;
					highlightTeamSelect = false;
					highlightaboutus = false;
				}
				else {
					highlight = false;
					highlightBack = false;
				}
			}
			

			
		}
		else if(game.isBegun() && onBoard(e.getX(), e.getY())) {
			
			
			
			//si elle est d�plac�e sur un emplacement valide avec le caract�re
			if(game.getPiece(e.getX(), e.getY()) != null) {
				abilityX = e.getX();
				abilityY = e.getY();
				ability = true;
			}
			else {
				ability = false;
			}
			//mettre l�g�rement en �vidence le carr� de la planche
			highlightX = e.getX();
			highlightY = e.getY();
			highlight = true;
			
		}
		else {
			if(game.isBegun()) {
				if (e.getX() > 1700 && e.getX() < 1875 && e.getY() > 850 && e.getY() < 912) {
					highlightSave = true;
					highlightBackGame = false;
				}else if(e.getX() > 1700 && e.getX() < 1875 && e.getY() > 913 && e.getY() < 975){
					highlightBackGame = true;
					highlightSave = false;
				}else {
					highlightSave = false;
					highlightBackGame = false;
				}
			}
			highlight = false;
		}
		repaint();
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent arg0) {
	}
}
