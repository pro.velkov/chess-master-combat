import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.ImageIcon;

/**
 * l'objet pi�ce de jeu
 * @author imad
 *
 */

public class GamePiece {
	private int health;
	private int level;
	private int x;
	private int y;
	private Ability ability;
	private int damage;
	private String team;
	private int maxHealth;
	private String imageURL;
	private String name;
	private int xp;
	private boolean selected;
	private int moveType;
	
	/**
	 * GamePiece constructor
	 * @param name le nom de la pi�ce
	 * @param team l'�quipe de la pi�ce
	 * @param x la coordonn�e X de la pi�ce
	 * @param y la coordonn�e Y de la pi�ce
	 * @param health les points de vie de la pi�ce
	 * @param damage les points de d�gats de la pi�ce
	 * @param level le niveau de la pi�ce
	 * @param ability les capacit� de la pi�ce
	 * @param imageURL l'image de la pi�ce
	 * @param moveType le type de mouvement de la pi�ce
	 */
	public GamePiece(String name, String team, int x, int y, int health, int damage, int level, Ability ability, String imageURL, int moveType) {
		this.team = team;
		this.name = name;
		this.health = health + health*(level/4);
		this.maxHealth = health + health*(level/4);
		this.level = level;
		this.damage = damage+damage*(level/3);
		this.x = x;
		this.y = y;
		this.ability = ability;
		this.imageURL = imageURL;
		this.xp = 0;
		this.selected = false;
		this.moveType = moveType;
	}
	/**
	 * Enregistre la pi�ce
	 * @return String des infos de la pi�ce
	 */
	public String save() {
		String data="";		
		data+=this.name ;
		data+=","+this.team;
		data+=","+this.getX() ;
		data+=","+this.getY() ;
		data+=","+this.health ;
		data+=","+this.damage ;
		data+=","+this.level ;
		data+=","+this.ability.save();
		data+='"'+this.imageURL+'"';
		data+=","+this.moveType+",";
		return data;
	}
	
	/**
	 * retourner le nom de la pi�ce (par exemple, "Archer")
	 * @return le nom de la pi�ce
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * renvoie la capacit� de la pi�ce sous la forme d'un objet Ability.
	 * @return la capacit� 
	 */
	public Ability getAbility() {
		return ability;
	}
	
	/**
	 * renvoie une repr�sentation enti�re du type de capacit�
	 * @return
	 */
	public int getAbilityType() {
		return ability.getType();
	}
	
	/**
	 * renvoie le nom de la capacit�
	 * @return la capacit�
	 */
	public String getAbilityName() {
		return ability.getName();
	}
	
	/**
	 * renvoie un tableau de cha�nes format�es contenant la description de la capacit�.
	 * @return la d�scription de la capacit� 
	 */
	public String[] getAbilityDescription() {
		return ability.getDescription();
	}
	
	/**
	 * met � jour la description de la capacit� de la pi�ce
	 * @param description la d�scription de la pi�ce
	 */
	public void updateAbilityDescription(String[] description) {
		ability.updateDescription(description);
	}
	
	/**
	 * renvoie une repr�sentation enti�re de l'�tat de sant� de la pi�ce
	 * @return
	 */
	public int getHealth(){
		return health;
	}
	
	/**
	 * renvoie une repr�sentation enti�re du niveau de la pi�ce
	 * @return le niveau
	 */
	public int getLevel() {
		return level;
	}
	
	/**
	 * met � jour le niveau du morceau en fonction de l'int qui lui est pass� (de 1 � 9)
	 * @param level le niveau 
	 */
	public void setLevel(int level) {
		this.level = level;
	}
	
	/**
	 * renvoie la coordonn�e x, de 0 � 15, de la pi�ce sur le plateau.
	 * @return la coordon�e x de la pi�ce 
	 */
	public int getX(){
		return x;
	}
	
	/**
	 * renvoie la coordonn�e y, de 0 � 11, de la pi�ce sur le plateau.
	 * @return la coordon�e Y de la pi�ce
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * renvoie l'image de la pi�ce
	 * @return image
	 */
	public ImageIcon getImage() {
		return new ImageIcon(imageURL);
	}
	
	/**
	 * d�finit l'image de la pi�ce (utilis�e si nous d�cidons de changer d'image pour repr�senter la mont�e de niveau)
	 * @param imageURL l'url de l'image
	 */
	public void setImage(String imageURL) {
		this.imageURL = imageURL;
	}
	
	/**
	 * renvoie vrai si la sant� de la pi�ce est strictement sup�rieure � 0
	 * @return l'�tat de la pi�ce
	 */
	public boolean isAlive() {
		return (getHealth() > 0);
	}

	/**
	 * met � jour la sant� de la pi�ce
	 * @param health les points de vie de la pi�ce
	 */
	public void setHealth(int health) {
		this.health = health;
	}

	/**
	 * met � jour les coordonn�es x de la pi�ce sur le tableau
	 * @param x les coordon�e X de la pi�ce
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * met � jour les coordonn�es y de la pi�ce sur le tableau
	 * @param y les coordonn�es Y de la pi�ce 
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * retourne le dommage de la pi�ce
	 * @return d�gats de la pi�ce 
	 */
	public int getDamage() {
		return damage;
	}
	
	/**
	 * met � jour les dommages de la pi�ce
	 * @param damage nouveau d�gats
	 */
	public void setDamage(int damage) {
		this.damage = damage;
	}
	
	/**
	 * renvoie la sant� initiale/maximale de la pi�ce - utilis�e pour les barres de sant�.
	 * @return sant� maximale de la pi�ce
	 */
	public int getMaxHealth() {
		return maxHealth;
	}

	/**
	 * d�finit les coordonn�es x et y de la pi�ce (utilis�e pour le d�placement) de 0 � 15 en x et de 0 � 11 en y.
	 * @param x2 Coord X de la pi�ce
	 * @param y2 Coord Y de la pi�ce
	 */
	public void setXY(int x2, int y2) {
		this.x = x2;
		this.y = y2;
	}

	/**
	 * renvoie une cha�ne de caract�res repr�sentant l'�quipe � laquelle appartient la pi�ce : "Rouge" ou "Bleu"
	 * @return la couleur de l'�quipe
	 */
	public String getTeam() {
		return team;
	}
	
	/**
	 * renvoie une repr�sentation enti�re de l'�quipe � laquelle appartient la pi�ce : 0 pour les bleus et 1 pour les rouges
	 * @return le nombre de l'�quipe
	 */
	public int getTeamAsInt() {
		if(team.equals("Red")) {
			return 1;
		}
		return 0;
	}
	
	/**
	 * returns the xp as an int
	 * @return le nombre d'XP
	 */
	public int getXP() {
		return xp;
	}
	
	/**
	 * renvoie le moveType de la pi�ce afin que nous puissions savoir comment elle va se d�placer : ligne, saut, un, etc.
	 * @return le type de mouvement
	 */
	public int getMoveType() {
		return moveType;
	}
	
	/**
	 * d�finit l'xp de la pi�ce et met � jour sa sant� et ses d�g�ts en fonction de ce score
	 * @param xp XP
	 */
	public void setXP(int xp) {
		this.xp = xp;
		if(xp<160) {
			this.setLevel(1 + xp/20);
			this.setDamage(damage+damage*(level/3));
			this.setHealth(health+health*(level/4));
			maxHealth = this.getHealth();
		}
		else {
			this.setLevel(9);
			this.setDamage(damage+damage*(level/3));
			this.setHealth(health+health*(level/4));
			maxHealth = this.getHealth();
		}
	}
	
	/**
	 * retourne vrai si la pi�ce est s�lectionn�e
	 * @return la pi�ce selectionner
	 */
	public boolean isSelected() {
		return selected;
	}
	
	/**
	 * s�lectionne la pi�ce pour l'importation si elle a �t� d�s�lectionn�e dans l'�cran "s�lectionner l'�quipe".
	 * @param piece la pi�ce � s�lectionner
	 * @param characters le type de pi�ce
	 * @param game la partie en cours
	 * @param board la table de jeu
	 */
	public void select(GamePiece piece, GamePiece[] characters, Game game, GamePiece[] board) {
		selected = true;
		try {
			writeSelection(piece, characters);
			reImport(piece, characters, board);
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
	}
	
	/**
	 * d�s�lectionne la pi�ce de l'importation si elle a d�j� �t� s�lectionn�e sur l'�cran "s�lectionner l'�quipe".
	 * @param piece la pi�ce � d�s�lectionner
	 * @param characters le type de pi�ce
	 * @param game la partie en cours
	 * @param board la table de jeu
	 */
	public void deselect(GamePiece piece, GamePiece[] characters, Game game, GamePiece[] board) {
		selected = false;
		try {
			writeSelection(piece, characters);
			reImport(piece, characters, board);
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
	}
	
	/**
	 * r�importer les pi�ces apr�s que de nouvelles ont �t� s�lectionn�es ou d�s�lectionn�es
	 * @param piece pi�ce dans la liste d'import
	 * @param characters le type de pi�ce
	 * @param board la table de jeu
	 */
	public void reImport(GamePiece piece, GamePiece[] characters, GamePiece[] board) {
		//imports the 10 pieces
		FileReader read1;
		try {
			read1 = new FileReader("importPieces.txt");
			Scanner sc1 = new Scanner(read1);
			int index = 0;
			while (sc1.hasNextLine() && index<10) {
				String lineArg = sc1.nextLine();
				board[index] = 	characters[Integer.parseInt(lineArg)];
				characters[Integer.parseInt(lineArg)].selected = true;
				index++;
			}
			sc1.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * �crit les morceaux s�lectionn�s dans un fichier
	 * @param piece la pi�ce
	 * @param characters les charact�re 
	 * @throws FileNotFoundException fichier inexistant
	 */
	public void writeSelection(GamePiece piece, GamePiece[] characters) throws FileNotFoundException {
		PrintWriter write = new PrintWriter("importPieces.txt");
		for(int i = 0; i < characters.length; ++i){
			if(characters[i].isSelected()) {
				write.write(Integer.toString(i) + "\n");
			}
		}
		write.close();
		}
}
