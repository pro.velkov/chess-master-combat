# RPG MASTER COMBAT 
## Un nouveau jeu créatif inspiré des échecs fait par : Kristiyan, Imad & Nasr.
lien présentation vidéo : https://www.youtube.com/watch?v=lbIbDaZ46qk

lien pdf conception : https://gitlab.com/pro.velkov/RPG_MC/-/blob/main/conception/conception_RPG_MC.pdf

- lancer eclipse --> File --> import --> Project from git (with smart import) --> clone URL
- copier ceci : https://gitlab.com/pro.velkov/chess-master-combat.git
- coller dans URL
- Entrez vos données
- Next --> Séléctionner RPG_Chess seulement

**Pour que vous profitiez pleinement du jeu, n'oubliez pas de modifier l'affichage de votre écran et le mettre à 100% au lieu de 125%.**
![image.png](./image.png)

## Une fois que tout est bon, lancez "GameMain" et amusez vous bien 😉
____________________________________________________________________________________________________

Aperçu du jeu :

# MENU PRINCIPAL
![menuPrincipal.png](./menuPrincipal.png)

# SELECT TEAMS
![plateauJeu.png](./plateauJeu.png)

# ABOUT US
![aboutUs.png](./aboutUs.png)
